'use strict';

let gulp        = require('gulp')
  , sass        = require('gulp-sass')
  , sourcemaps  = require('gulp-sourcemaps')
  , gutil       = require('gulp-util')
  , browserSync = require('browser-sync')
  , browserify  = require('browserify')
  , source      = require('vinyl-source-stream')
;

gulp
  .task('browserify', function() {
    gutil.log('Compiling JS....')
    browserify('src/main.js')
      .transform('babelify', { 
        presets: ["es2015","stage-1","stage-0","react"],
        plugins: ['transform-runtime','transform-decorators-legacy']
      })
      .bundle()
      .on('error', function (err) {
          gutil.log(err.message)
          browserSync.notify("Browserify Error! :", err)
          this.emit("end")
        })
      .pipe(source('main.js'))
      .pipe(gulp.dest('public/js'))
      .pipe(browserSync.stream({once: true})
    )
  })

  .task('sass', function() {
    return gulp.src('src/assets/sass/style.scss')
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('public/styles'));
  })

  .task('sync', function() {
    browserSync.init({
      server: 'public'
     ,port: 8000
    })
  })

  .task('default', ['browserify', 'sass', 'sync'], function() {
    gulp.watch('src/**/*.*', ['browserify'])
    gulp.watch('src/assets/**/*.*', ['sass'])
    gulp.watch('public/**/*.*', function() { browserSync.reload() })
  })
;

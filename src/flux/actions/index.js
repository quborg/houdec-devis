import alt from '../alt';


class Actions {

  setClient (client) {
    return (dispatch) => {
      dispatch(client)
    }
  }

  setProducts (products) {
    return (dispatch) => {
      dispatch ( products )
    }
  }

}

module.exports = alt.createActions(Actions);

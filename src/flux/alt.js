import Alt from 'alt';

export default new Alt(); // this instantiates a Flux dispatcher for you and gives you methods to create your actions and stores.

'use strict'

import React            from 'react';
import WoodsList        from '../templates/WoodsList';
import Actions          from '../../actions';
import Store            from '../../stores';
import connectToStores  from 'alt-utils/lib/connectToStores';

import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import { Col, Table, Form, FormGroup, FormControl, Button, Glyphicon, Checkbox, Collapse } from 'react-bootstrap';


@connectToStores
export default class DoorComponent extends React.Component {

  constructor (props,context) {
    super(props,context);
    this.state = {
      index: props.index,
      coversBox: true,
      products: props.products,
      structure: context.structure
    };
  }

  static contextTypes = { structure: React.PropTypes.object }

  static getStores () { return [Store] }

  static getPropsFromStores () { return Store.getState() }

  componentWillReceiveProps (nextProps) {
    if (this.state.products !== nextProps.products)
      this.setState({ products: nextProps.products })
  }

  coversBoxHandler = () => {
    this.setState({coversBox: !this.state.coversBox})
  }

  render () {
    let {index, products, structure}    = this.state
      , {propertyHandler,coverHandler}  = this.props
      , product                         = products[index]
      , components = [
          {label:'Panneau', name:'panel'},
          {label:'Cadre',   name:'frame'},
          {label:'Chambre', name:'room' }
        ]
      ;
    return (
      <span>
        {
          structure.door.map( (component,c) => {
            let {name, fields}  = component
              , compon          = product[name]
            ;
            return (
              <Table key={c} className="calculator door">
                <thead>
                  <tr>
                    { fields.map( (field,f) => { return <th key={f} style={field.style}>{field.thead}</th> }) }
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    {
                      fields.map( (field,f) => {
                        switch (field.type) {
                          case 'label':
                            return  <td key={f}>{field.text}</td>
                            break;
                          case 'woodlist':
                            return  <td key={f}><WoodsList value={compon.wood} component={name} onChange={propertyHandler}/></td>
                            break;
                          case 'number':
                            return  <td key={f}><FormControl
                                      type="number"
                                      min={field.min}
                                      step={field.step}
                                      value={compon[field.property]}
                                      onChange={propertyHandler.bind(this,name,field.property)}/></td>
                            break;
                          case 'margin':
                            return  <td key={f}><FormControl
                                      type="number"
                                      max={field.max}
                                      min={field.min}
                                      step={field.step}
                                      value={compon[field.property]}
                                      onChange={propertyHandler.bind(this,name,field.property)}/></td>
                            break;
                        }
                      })
                    }
                  </tr>
                </tbody>
              </Table>
            )
          })
        }
        <div>
          <Button onClick={this.coversBoxHandler}>
            <Glyphicon glyph="tint"/>
            Couverture du bois
          </Button>
          <Collapse in={this.state.coversBox}>
            <div className="cover-options">
              {
                components.map( (component,c) => {
                  return (
                    <Col xs={4} key={c}>
                      <label>{component.label}</label>
                      <RadioButtonGroup
                        name="cover"
                        defaultSelected={product[component.name].cover}
                        valueSelected={product[component.name].cover}
                        onChange={coverHandler.bind(this,index,component.name)}>
                          <RadioButton value="" label="Sans" />
                          <RadioButton value="brillon" label="Peinture Brillon"/>
                          <RadioButton value="matte" label="Peinture Matté"/>
                          <RadioButton value="vernis" label="Vernis" />
                      </RadioButtonGroup>
                    </Col>
                  )
                })
              }
            </div>
          </Collapse>
        </div>
      </span>
    )
  }

}

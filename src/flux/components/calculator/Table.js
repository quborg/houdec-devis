'use strict'

import React            from 'react';
import Assign           from 'object-assign';
import Door            from './Door';


export default class TableComponent extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      type: '',
      index: null
    };
  }

  componentWillReceiveProps (nextProps) {
    let {type,_index} = this.state
      , {_type,index} = nextProps;
    if (type!==_type && index!==_index)
      this.setState({
        type: nextProps.type,
        index: nextProps.index
      })
  }

  render () {
    let { type, index }                 = this.state
      , {propertyHandler, coverHandler} = this.props;
    switch (type) {
      case 'door':
        return <Door index={index} propertyHandler={propertyHandler} coverHandler={coverHandler}/>
        break;
      default:
        return <div>Veillez choisir <b>le type du produit</b></div>
        break;
    }
  }

}

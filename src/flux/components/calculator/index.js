'use strict'

import React            from 'react';
import Table            from './Table';
import Invoice          from '../invoice';
import Store            from '../../stores';
import Actions          from '../../actions';
import Gen              from '../../../services/Gen';
import connectToStores  from 'alt-utils/lib/connectToStores';

import { Row, Col, Panel, Form, FormGroup, FormControl, Jumbotron, Well, Button, Glyphicon } from 'react-bootstrap';


@connectToStores
export default class CalculatorComponent extends React.Component {

  constructor (props,context) {
    super(props,context);
    this.state = {
      show: false,
      client: props.client,
      products: props.products
    };
  }

  static getStores () { return [Store] }

  static getPropsFromStores () { return Store.getState() }

  clientHandler = (e) => {
    this.setState({ client: e.target.value});
    Actions.setClient(e.target.value);
  }

  productsQuantityHandler = (e) => {
    let products  = Object.assign([], this.state.products)
      , _old      = products.length
      , _new      = e.target.value
      , _products = []
      ;
    if (_new < _old) {
      _products = products.slice(0, _new);
      this.setState({products: _products});
      Actions.setProducts(_products);
    }
    if (_new > _old) {
      while (_new > _old) {
        products.push({type:'', designation:'Porte en bois hetre', margin: 0.5, mainoeuvre: 0});
        _old++
      }
      this.setState({products: products});
      Actions.setProducts(products);
    }
  }

  designationHandler = (d,e) => {
    let products = Object.assign([], this.state.products);
    products[d].designation = e.target.value;
    this.setState({ products: products});
    Actions.setProducts(products);
  }

  marginHandler = (d,e) => {
    let products = Object.assign([], this.state.products);
    products[d].margin = e.target.value;
    this.setState({ products: products});
    Actions.setProducts(products);
  }

  mainoeuvreHandler = (d,e) => {
    let products = Object.assign([], this.state.products);
    products[d].mainoeuvre = e.target.value;
    this.setState({ products: products});
    Actions.setProducts(products);
  }

  typeProductHandler = (p,e) => {
    let products  = Object.assign([], this.state.products)
      , type      = e.target.value
      , product   = Object.assign({}, products[p], Gen[type], {type:type})
      ;
    products[p] = product;
    this.setState({products: products});
    Actions.setProducts(products);
  }

  propertyHandler = (i,c,p,e) => {
    let products  = Object.assign([], this.state.products)
      , product   = Object.assign({},products[i])
      , component = Object.assign({},product[c])
      ;
    component[p] = e.target.value;
    product[c]   = component;
    products[i]  = product;
    this.setState({ products: products});
    Actions.setProducts(products);
  }

  coverHandler = (p,c,e,v) => {
    let products  = Object.assign([], this.state.products)
      , product   = products[p]
      ;
    product[c].cover = v;
    products[p]      = product;
    this.setState({products: products});
    Actions.setProducts(products);
  }

  render () {
    let {show, client, products} = this.state;
    return (
      <Row id="calculator">
        <Col xs={12}>
          <Form horizontal>
            <Well>
              <FormGroup>
                <Col xs={1} xsOffset={1} componentClass="label" className="control-label">
                  Client
                </Col>
                <Col xs={5}>
                  <FormControl
                    type="text"
                    placeholder="Entrez le nom du client"
                    value={client}
                    onChange={this.clientHandler}/>
                </Col>
                <Col xs={2} componentClass="label" className="control-label">
                  Nombre de produits
                </Col>
                <Col xs={1}>
                  <FormControl
                    type="number"
                    min={0}
                    step={1}
                    value={products.length}
                    onChange={this.productsQuantityHandler}/>
                </Col>
              </FormGroup>
            </Well>
            <Jumbotron>
              {
                products.map( (product,p) => {
                  let {type} = product;
                  return (
                    <Panel key={p}>
                      <FormGroup>
                        <div className="header-des">
                          <Col xs={2} componentClass="label" className="control-label">Type de Produit</Col>
                          <Col xs={4}>
                            <FormControl componentClass="select" value={type} onChange={this.typeProductHandler.bind(this,p)}>
                              <option value="-"></option>
                              <option value="door">Porte</option>
                              <option value="cupboard">Placard</option>
                              <option value="table">Table</option>
                              </FormControl>
                          </Col>
                          <Col xs={2} componentClass="label" className="control-label">Désignation</Col>
                          <Col xs={4}>
                            <FormControl
                              type="text"
                              placeholder="Entrez le nom du produit"
                              value={product.designation}
                              onChange={this.designationHandler.bind(this,p)}/>
                          </Col>
                        </div>
                        <Col xs={12}>
                          <Table
                            type={type}
                            index={p}
                            propertyHandler={this.propertyHandler.bind(this,p)}
                            coverHandler={this.coverHandler}/>
                        </Col>
                        <Col xs={2} componentClass="label" className="control-label">La Marge</Col>
                        <Col xs={2}>
                          <FormControl
                            type="number"
                            value={product.margin}
                            min={0}
                            max={1}
                            onChange={this.marginHandler.bind(this,p)}/>
                        </Col>
                        <Col xs={3} componentClass="label" className="control-label">La main d'oeuvre</Col>
                        <Col xs={2}>
                          <FormControl
                            type="number"
                            value={product.mainoeuvre}
                            min={0}
                            onChange={this.mainoeuvreHandler.bind(this,p)}/>
                        </Col>
                      </FormGroup>
                    </Panel>
                  )
                })
              }
            </Jumbotron>
            <Button
              bsStyle="info"
              onClick={()=>this.setState({show:true})}
              style={{width:200,float:'right',marginBottom:20}}>
              <Glyphicon glyph="file"/>FACTURE
              </Button>
          </Form>
          <Invoice
            show={show}
            client={client}
            products={products}
            hideCallback={()=>{this.setState({show:false})}}/>
        </Col>
      </Row>
    )
  }

}

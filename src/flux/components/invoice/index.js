'use strict'

import React            from 'react';
import Helper           from '../../../utils/Helper';
import { Row, Col, FormControl, Modal, Table, Button, Glyphicon } from 'react-bootstrap';
import Store            from '../../stores';
import connectToStores  from 'alt-utils/lib/connectToStores';

let components = ['panel','frame','room'];


@connectToStores
export default class DevisComponent extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      show: false,
      docType: 'DEVIS',
      gender : 'Monsieur',
      client: '',
      products: [],
      totalPanel: 0,
      totalFrame: 0,
      totalRoom: 0,
      total: 0,
      totalAmount: 0,
      discount: 0,
      discountAmount: 0.00
    };
  }

  static getStores () { return [Store] }

  static getPropsFromStores () { return Store.getState() }

  componentWillReceiveProps (nextProps) {
    let s = this.state, n = nextProps;
    if (n.show !== s.show)
      this.setState({show: n.show})
    if (n.client !== s.client)
      this.setState({client: n.client})
    if (n.products !== s.products)
      this.setState({products: n.products}, this.calculateTotal(n.products))
  }

  calculateTotal = (products) => {
    let total = 0;
    products.map( (product,p) => {
      if (product.type) {
        // let totalDoor = 0
        switch (product.type) {
          case 'door':
            components.map( (component,c) => {
              console.log(component,product[component])
              switch (component) {
                case 'panel':
                  let result = Number(Helper.Amount.panel(product[component]));
                  this.setState({totalPanel:result});
                  total += result;
                break;
                case 'frame':
                  let result = Number(Helper.Amount.frame(product[component]));
                  this.setState({totalFrame:Number(Helper.Amount.panel(product[component]))});
                  total += result;
                break;
                case 'room':
                  let result = Number(Helper.Amount.room(product[component]));
                  this.setState({totalRoom:Number(Helper.Amount.panel(product[component]))});
                  total += result;
                break;
              }
            })
          break;
        }
        total = (total * (1+Number(product.margin))) + Number(product.mainoeuvre)
        console.log('total complet :',total)
      }
    })
    let _discountAmount = (total*this.state.discount/100).toFixed(2);
    this.setState({
      total: total,
      discountAmount: _discountAmount
    });
    this.calculateTotalAmount(total,_discountAmount);
  }

  calculateTotalAmount = (total,discount) => {
    this.setState({totalAmount: total - discount})
  }

  hide = () => {
    this.props.hideCallback()
    this.setState({show: false})
  }

  getDate () {
    let Y     = new Date().getFullYear()
      , M     = new Date().getMonth() + 1
      , D     = new Date().getDate()
    ;
    return (D<10?'0'+D:D)+'/'+(M<10?'0'+M:M)+'/'+Y;
  }

  discountHandler = (e) => {
    let _discountAmount = (this.state.total*e.target.value/100).toFixed(2);
    this.setState({
      discount: e.target.value,
      discountAmount: _discountAmount
    }, this.calculateTotalAmount(this.state.total, _discountAmount));
  }

  print () { window.print() }

  render () {
    let {show, docType, gender, client, products, totalPanel, totalFrame, totalRoom, total, totalAmount, discount, discountAmount} = this.state;
    return (
      <Modal show={show} onHide={this.hide} id="invoice" className={docType+" container"}>
        <Modal.Body>
          <Row>
            <Col xs={12} className="text-right">
              Casablanca le : {this.getDate()}
            </Col>
            <Col xs={12} className="devis-number f24">
              <FormControl componentClass="select" value={docType} onChange={(e)=>this.setState({docType:e.target.value})} className="no-print optimize inline f24 w125">
                <option value="FACTURE">FACTURE</option>
                <option value="DEVIS">DEVIS</option>
                </FormControl>
              <span className="print-inline f24">{docType}</span> N°: <input type="text" defaultValue="AC-092016"/>
            </Col>
            <Col xs={12} className="devis-client-name">
              <Col xs={2} xsOffset={3} className="text-right"><i>Client :</i></Col>
              <Col xs={7} className="client-name">{client||'Client Houdec'}</Col>
            </Col>
            <Col xs={12} className="introduction">
              <p>
                <FormControl componentClass="select" value={gender} onChange={(e)=>this.setState({gender:e.target.value})} className="no-print optimize f18 w100">
                  <option value="Monsieur">Monsieur</option>
                  <option value="Madame">Madame</option>
                  </FormControl>
                <i className="print-inline f18">{gender}</i>
              </p>
              <p>Nous avons le plaisir de vous communiquer notre offre de prix pour les articles suivants:</p>
            </Col>
            <Col xs={12}>
              <Table>
                <thead>
                  <tr>
                    <th width="30%">Désignation</th>
                    <th width="10%">Unité</th>
                    <th width="20%">P.U. H.T.</th>
                    <th width="20%">Mesure</th>
                    <th width="20%">MT H.T.</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    products.map( (product,d) => {
                      if (product.type) {
                        return ( components.map( (component,i) => {
                          let {unity,length,width,door_width,wood,cover} = product[component];
                          return (
                            <tr key={i}>
                              { !i && (<td rowSpan={component=='panel'?3:1} style={{verticalAlign:'middle',textTransform:'uppercase'}}>{product.designation}</td>) }
                              <td className="text-center">{unity}</td>
                              <td className="text-right bold">{(componentTotal[component]+Number(Helper.Prices.Covers[cover]))*(Number(product.margin)+1).toFixed(2)||0.00}</td>
                              <td className="text-center">{length}*{width||door_width}</td>
                              <td className="text-right bold">{((componentTotal[component]+Number(Helper.Prices.Covers[cover]))*(Number(product.margin)+1)*unity).toFixed(2)||0.00}</td>
                            </tr>
                          )
                        }))
                      }
                    })
                  }
                </tbody>
                <tfoot>
                  <tr>
                    <td rowSpan={3} colSpan={2} className="td-indent"></td>
                    <td colSpan={2} className="text-center">TOTAL H.T</td>
                    <td className="text-right">{total.toFixed(2)}</td>
                  </tr>
                  <tr>
                    <td colSpan={2} className="text-center discount">
                      REMISE
                      <input className="no-print text-center" type="number" value={discount} step="0.01" min="0" max="100" onChange={this.discountHandler}/>
                      <span className="print-inline text-center">{discount}</span>
                      %
                    </td>
                    <td className="text-right">{discountAmount}</td>
                  </tr>
                  <tr>
                    <td colSpan={2} className="text-center">TOTAL</td>
                    <td className="text-right">{totalAmount.toFixed(2)}</td>
                  </tr>
                </tfoot>
              </Table>
            </Col>
            <Col xs={12} className="conclusion">
              <p>Nous restons à votre disposition pour toute information</p>
            </Col>
            <Col xs={12}>
              <div className="amount-in-words text-center">Arrêter le présent devis à la somme de : <span>{Helper.NumberToWords(totalAmount)} Dirhams et ZERO CTS TTC.</span></div>
            </Col>
            <Col xs={8} xsOffset={4} className="text-center signature"><small>Signature client</small></Col>
          </Row>
        </Modal.Body>
        <Button bsStyle="success" onClick={this.print} className="imprimer no-print"><Glyphicon glyph="print"/>Imprimer</Button>
      </Modal>
    )
  }

}

'use strict'

import React            from 'react';
import { FormControl }  from 'react-bootstrap';


export default class WoodsListComponent extends React.Component {
  render () {
    let {value,component,onChange} = this.props
    return (
      <FormControl componentClass="select" value={value} onChange={onChange.bind(this,component,'wood')} style={{minWidth:200}}>
        <option value="-"></option>
        <optgroup label="MASSIF">
          <option value="m-acajou">Acajou massif</option>
          <option value="m-cedre">Cedre massif</option>
          <option value="m-chene">Chene massif</option>
          <option value="m-eroco">Eroco massif</option>
          <option value="m-frene">Frene massif</option>
          <option value="m-hetre">Hetre massif</option>
          <option value="m-noye">Noyé massif</option>
          <option value="m-wangue">Wangué massif</option>
        </optgroup>
        <optgroup label="ISOPLAIN">
          <option value="i-acajou">Acajou isoplain</option>
          <option value="i-cedre">Cedre isoplain</option>
          <option value="i-chene">Chene isoplain</option>
          <option value="i-eroco">Eroco isoplain</option>
          <option value="i-frene">Frene isoplain</option>
          <option value="i-hetre">Hetre isoplain</option>
          <option value="i-noye">Noyé isoplain</option>
          <option value="i-wangue">Wangué isoplain</option>
        </optgroup>
      </FormControl>
    )
  }
}

import alt              from '../alt';
import Actions          from '../actions';
import {decorate, bind} from 'alt-utils/lib/decorators';


@decorate(alt)
class ContactsStore {

  constructor () {
    this.state = {
      client: 'Mme Zineb',
      products: []
    };
  }

  @bind(Actions.setProducts)
  setProducts (products) {
    this.setState({products: products});
  }

  @bind(Actions.setClient)
  setClient (client) {
    this.setState({client: client});
  }

}

export default alt.createStore(ContactsStore);

export default {
  door: {
    panel: {
      unity: 1,
      length: 2,
      width: 1,
      thickness: 0.04,
      wood: 'm-hetre',
      cover: 'vernis'
    },
    frame: {
      unity: 1,
      length: 2,
      door_width: 1,
      frame_width: 0.02,
      thickness: 0.015,
      wood: 'm-hetre',
      cover: 'vernis'
    },
    room: {
      unity: 1,
      length: 2,
      door_width: 1,
      room_width: 0.02,
      thickness: 0.015,
      wood: 'm-hetre',
      cover: 'vernis'
    }
  }
}
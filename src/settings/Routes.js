'use strict';

import React      from 'react';
import Template   from '../flux/components/templates';
import Calculator from '../flux/components/calculator';

import { Router, Route, IndexRoute, browserHistory } from 'react-router';

module.exports = (
  <Router history={browserHistory}>
    <Route path="/" component={Template}>
      <IndexRoute component={Calculator}/>
    </Route>
  </Router>
)

'use strict'

module.exports = {

  app_name: '<span class="logo-name">HOUDEC</span> <small>facturation</small>',

  door: [
    {
      name: 'panel',
      fields: [
        {
          type: 'label',
          thead: '',
          text: 'Panneau'
        },
        {
          type: 'number',
          thead: 'Unité',
          style: {width:75},
          property: 'unity',
          min: 1,
          step: 1
        },
        {
          type: 'number',
          thead: 'Longueur',
          property: 'length',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Largeur',
          property: 'width',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Epaisseur',
          property: 'thickness',
          min: 0,
          step: 0.01
        },
        {
          type: 'woodlist',
          thead: 'Bois'
        }
      ]
    },
    {
      name: 'frame',
      fields: [
        {
          type: 'label',
          thead: '',
          text: 'Cadre'
        },
        {
          type: 'number',
          thead: 'Unité',
          style: {width:75},
          property: 'unity',
          min: 1,
          step: 1
        },
        {
          type: 'number',
          thead: 'Longueur',
          property: 'length',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Largeur Porte',
          property: 'door_width',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Largeur Cadre',
          property: 'frame_width',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Epaisseur',
          property: 'thickness',
          min: 0,
          step: 0.01
        },
        {
          type: 'woodlist',
          thead: 'Bois'
        }
      ]
    },
    {
      name: 'room',
      fields: [
        {
          type: 'label',
          thead: '',
          text: 'Chambre'
        },
        {
          type: 'number',
          thead: 'Unité',
          style: {width:75},
          property: 'unity',
          min: 1,
          step: 1
        },
        {
          type: 'number',
          thead: 'Longueur',
          property: 'length',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Largeur Porte',
          property: 'door_width',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Largeur Chambre',
          property: 'room_width',
          min: 0,
          step: 0.01
        },
        {
          type: 'number',
          thead: 'Epaisseur',
          property: 'thickness',
          min: 0,
          step: 0.01
        },
        {
          type: 'woodlist',
          thead: 'Bois'
        }
      ]
    }
  ]
}

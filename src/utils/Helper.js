let Prices = {
      Woods: {
        '-'         :      0.00,
        'm-acajou'  :   1400.00,
        'm-cedre'   :  17000.00,
        'm-chene'   :  14500.00,
        'm-eroco'   :      0.00,
        'm-frene'   :  14500.00,
        'm-hetre'   :   7800.00,
        'm-noye'    :      0.00,
        'm-wangue'  :  25000.00,
        'i-acajou'  :      0.00,
        'i-cedre'   :      0.00,
        'i-chene'   :      0.00,
        'i-eroco'   :      0.00,
        'i-frene'   :      0.00,
        'i-hetre'   :      0.00,
        'i-noye'    :      0.00,
        'i-wangue'  :      0.00
      },
      Covers: {
        'brillon': 1000.00,
        'matte'  : 880.00,
        'vernis' : 700.00
      }
    }
;

module.exports = {

  Unities ( number ) {
    let unites = {0:"zéro",1:"un",2:"deux",3:"trois",4:"quatre",5:"cinq",6:"six",7:"sept",8:"huit",9:"neuf"};
    return unites[number];
  },

  Tens ( number ) {
    let tens = {10:"dix",11:"onze",12:"douze",13:"treize",14:"quatorze",15:"quinze",16:"seize",17:"dix-sept",18:"dix-huit",19:"dix-neuf",20:"vingt",30:"trente",40:"quarante",50:"cinquante",60:"soixante",70:"soixante-dix",80:"quatre-vingt",90:"quatre-vingt-dix"}
    return tens[number];
  },

  NumberToWords ( number ) {
    let i, j, n, quotient, reste, nb, ch, numberToLetter = '';

    if ( number.toString().replace(/ /gi, "").length > 9) return "overflow capacity"; // true is > 15, but now is 9
    if ( isNaN(number.toString().replace(/ /gi, "")) ) return "number not valide";

    nb = parseFloat(number.toString().replace(/ /gi, ""));
    if ( Math.ceil(nb) != nb ) return  "Number with comma not allowed.";

    n = nb.toString().length;

    switch ( n ) {
      case 1: numberToLetter = this.Unities(nb); break;
      case 2:
        if ( nb > 19 ) {
          quotient = Math.floor(nb / 10);
          reste = nb % 10;
          if ( nb < 71 || (nb > 79 && nb < 91) ){
            if ( reste == 0 ) numberToLetter = this.Tens(quotient * 10);
            if ( reste == 1 ) numberToLetter = this.Tens(quotient * 10) + " et " + this.Unities(reste);
            if ( reste > 1 ) numberToLetter = this.Tens(quotient * 10) + " " + this.Unities(reste);
          } else numberToLetter = this.Tens((quotient - 1) * 10) + " " + this.Tens(10 + reste);
        } else numberToLetter = this.Tens(nb);
        break;
      case 3:
        quotient = Math.floor(nb / 100);
        reste = nb % 100;
        if ( quotient == 1 && reste == 0 ) numberToLetter = "cent";
        if ( quotient == 1 && reste != 0 ) numberToLetter = "cent" + " " + this.NumberToWords(reste);
        if ( quotient > 1 && reste == 0 ) numberToLetter = this.Unities(quotient) + " cents";
        if ( quotient > 1 && reste != 0 ) numberToLetter = this.Unities(quotient) + " cent " + this.NumberToWords(reste);
        break;
      case 4 :  quotient = Math.floor(nb / 1000);
        reste = nb - quotient * 1000;
        if ( quotient == 1 && reste == 0 ) numberToLetter = "mille";
        if ( quotient == 1 && reste != 0 ) numberToLetter = "mille" + " " + this.NumberToWords(reste);
        if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " mille";
        if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " mille " + this.NumberToWords(reste);
        break;
      case 5 :  quotient = Math.floor(nb / 1000);
        reste = nb - quotient * 1000;
        if ( quotient == 1 && reste == 0 ) numberToLetter = "mille";
        if ( quotient == 1 && reste != 0 ) numberToLetter = "mille" + " " + this.NumberToWords(reste);
        if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " mille";
        if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " mille " + this.NumberToWords(reste);
        break;
      case 6 :  quotient = Math.floor(nb / 1000);
        reste = nb - quotient * 1000;
        if ( quotient == 1 && reste == 0 ) numberToLetter = "mille";
        if ( quotient == 1 && reste != 0 ) numberToLetter = "mille" + " " + this.NumberToWords(reste);
        if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " mille";
        if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " mille " + this.NumberToWords(reste);
        break;
      case 7: quotient = Math.floor(nb / 1000000);
        reste = nb % 1000000;
        if ( quotient == 1 && reste == 0 ) numberToLetter = "un million";
        if ( quotient == 1 && reste != 0 ) numberToLetter = "un million" + " " + this.NumberToWords(reste);
        if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " millions";
        if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " millions " + this.NumberToWords(reste);
        break;
      case 8: quotient = Math.floor(nb / 1000000);
        reste = nb % 1000000;
        if ( quotient == 1 && reste == 0 ) numberToLetter = "un million";
        if ( quotient == 1 && reste != 0 ) numberToLetter = "un million" + " " + this.NumberToWords(reste);
        if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " millions";
        if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " millions " + this.NumberToWords(reste);
        break;
      case 9: quotient = Math.floor(nb / 1000000);
        reste = nb % 1000000;
        if ( quotient == 1 && reste == 0 ) numberToLetter = "un million";
        if ( quotient == 1 && reste != 0 ) numberToLetter = "un million" + " " + this.NumberToWords(reste);
        if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " millions";
        if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " millions " + this.NumberToWords(reste);
        break;
      // case 10: quotient = Math.floor(nb / 1000000000);
      //   reste = nb - quotient * 1000000000;
      //   if ( quotient == 1 && reste == 0 ) numberToLetter = "un milliard";
      //   if ( quotient == 1 && reste != 0 ) numberToLetter = "un milliard" + " " + this.NumberToWords(reste);
      //   if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " milliards";
      //   if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " milliards " + this.NumberToWords(reste);
      //   break;
      // case 11: quotient = Math.floor(nb / 1000000000);
      //   reste = nb - quotient * 1000000000;
      //   if ( quotient == 1 && reste == 0 ) numberToLetter = "un milliard";
      //   if ( quotient == 1 && reste != 0 ) numberToLetter = "un milliard" + " " + this.NumberToWords(reste);
      //   if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " milliards";
      //   if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " milliards " + this.NumberToWords(reste);
      //   break;
      // case 12: quotient = Math.floor(nb / 1000000000);
      //   reste = nb - quotient * 1000000000;
      //   if ( quotient == 1 && reste == 0 ) numberToLetter = "un milliard";
      //   if ( quotient == 1 && reste != 0 ) numberToLetter = "un milliard" + " " + this.NumberToWords(reste);
      //   if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " milliards";
      //   if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " milliards " + this.NumberToWords(reste);
      //   break;
      // case 13: quotient = Math.floor(nb / 1000000000000);
      //   reste = nb - quotient * 1000000000000;
      //   if ( quotient == 1 && reste == 0 ) numberToLetter = "un billion";
      //   if ( quotient == 1 && reste != 0 ) numberToLetter = "un billion" + " " + this.NumberToWords(reste);
      //   if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " billions";
      //   if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " billions " + this.NumberToWords(reste);
      //   break;
      // case 14: quotient = Math.floor(nb / 1000000000000);
      //   reste = nb - quotient * 1000000000000;
      //   if ( quotient == 1 && reste == 0 ) numberToLetter = "un billion";
      //   if ( quotient == 1 && reste != 0 ) numberToLetter = "un billion" + " " + this.NumberToWords(reste);
      //   if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " billions";
      //   if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " billions " + this.NumberToWords(reste);
      //   break;
      // case 15: quotient = Math.floor(nb / 1000000000000);
      //   reste = nb - quotient * 1000000000000;
      //   if ( quotient == 1 && reste == 0 ) numberToLetter = "un billion";
      //   if ( quotient == 1 && reste != 0 ) numberToLetter = "un billion" + " " + this.NumberToWords(reste);
      //   if ( quotient > 1 && reste == 0 ) numberToLetter = this.NumberToWords(quotient) + " billions";
      //   if ( quotient > 1 && reste != 0 ) numberToLetter = this.NumberToWords(quotient) + " billions " + this.NumberToWords(reste);
      //   break;
    }

    if ( numberToLetter.substr(numberToLetter.length-"quatre-vingt".length,"quatre-vingt".length) == "quatre-vingt" ) numberToLetter = numberToLetter + "s";

    return numberToLetter;
  },

  Prices: Prices,

  Amount : {
    panel: (component) => {
      let {unity,length,width,thickness,wood,margin,cover} = component
        , woodAmount = Number(length)*Number(width)*Number(thickness)*Number(Prices.Woods[wood])
        // , coverAmount = ((2*length*width)+(2*length*thickness)+(2*width*thickness))*Prices.Covers[cover]
        , coverAmount = Prices.Covers[cover]
        ;
        console.log('panel',woodAmount,coverAmount,woodAmount+coverAmount)
      return cover ? unity*(woodAmount+coverAmount) : woodAmount ;
    },
    frame: (component) => {
      let {unity,length,door_width,frame_width,thickness,wood,margin,cover} = component
        , woodAmount = (2*Number(length)+Number(door_width))*Number(frame_width)*Number(thickness)*Number(Prices.Woods[wood])
        // , coverAmount = ((2*(2*length+door_width)*frame_width)+(2*(2*length+door_width)*thickness)+(2*thickness*frame_width))*Prices.Covers[cover]
        // , coverAmount = Prices.Covers[cover]
        ;
        console.log('frame',component,woodAmount)
      return unity * woodAmount ;
    },
    room: (component) => {
      let {unity,length,door_width,room_width,thickness,wood,margin,cover} = component
        , woodAmount = (2*Number(length)+Number(door_width))*Number(room_width)*Number(thickness)*Number(Prices.Woods[wood])*2
        // , coverAmount = ((2*(2*length+door_width)*room_width)+(2*(2*length+door_width)*thickness)+(2*thickness*room_width))*Prices.Covers[cover]*2
        // , coverAmount = Prices.Covers[cover]
        ;
        console.log('room',woodAmount)
      return unity * woodAmount ;
    }
  }

}
